package tk.mdogx.concept_map.tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import tk.mdogx.concept_map.selenium.Utils;
import tk.mdogx.concept_map.selenium.pages.ConceptMapPage;

import java.util.concurrent.TimeUnit;

public class ConceptMapTest {

    // for further extensibility, use a specific driver for each page instance

    // TODO: add BDD scenarios support with Cucumber
    // TODO: split test preparations step and scenarios

    private WebDriver driver;

    @BeforeTest
    public void setup() {
        driver = Utils.getDriver();
        driver.manage().timeouts().implicitlyWait(Utils.TIMEOUT, TimeUnit.SECONDS);
        driver.navigate().to(Utils.BASEURL);
        driver.manage().window().maximize();
    }

    @AfterTest
    public void testDown() {
        driver.quit();
    }

    // Check main page according to list of items
    @Test
    public void checkConceptMapPageData() {
        new ConceptMapPage(driver)
                .checkThatAllEpisodesAreVisible()
                .checkThatAllNodesAreVisible();
    }

    // Check that on specific episode page is displayed proper nodes
    @Test
    public void checkSpecificEpisodePageData() {
        new ConceptMapPage(driver)
                .openEpisodePage("Tim Cannon")
                .checkThatNumberOfNodesAreVisible(12)
                .checkEpisodeInformation("Tim Cannon is a co-founder of Grindhouse Wetware, a group of open-source biohackers in Pittsburg, Pennsylvania.");
    }

    // Check that on specific episode page is displayed proper nodes
    //TODO: should improve readability of page content checks
    @Test
    public void checkSpecificEpisodeDetailPageData() {
        new ConceptMapPage(driver)
                .openEpisodePage("Claire Evans")
                .checkThatNumberOfNodesAreVisible(7)
                .openDetailEpisodePageWithEpisodeNameLink()
                .checkThatCurrentEpisodeDetailPageMatchItem()
                .checkThatContentOfEpisodeDetailPageMatchExpected("Claire Evans is half of YACHT, a “band, business, and belief system” started by Jona Bechtolt in 2002. In addition to her musical/artistic adventures, she’s also a writer and regular science blogger. Unlike most bands, YACHT has a developed a detailed and public philosophy (read their FAQ or visit the YACHT Trust for more details) and they regularly explore ideas about the future in their work. I was especially intrigued by the themes of utopia and dystopia which tie together their album Shangri-La.")
                .checkThatTagsOfEpisodeDetailPageMatchExpected("Tags: art, curiosity, empowerment, future, human nature, knowledge, marfa mystery lights, music, mystery, relativism, science, transhumanism, YACHT");
    }

    // Check that on specific node page is displayed proper episodes
    @Test
    public void checkSpecificNodePageData() {
        new ConceptMapPage(driver)
                .openNodePage("Centralized")
                .checkThatNumberOfEpisodesAreVisible(11)
                .checkThatNodePairMatchExpected("(vs. Localized)");
    }

    // Surfing from the main page to the page with details about John Fife and back
    @Test
    public void checkNavigation() {
        new ConceptMapPage(driver)
                .openEpisodePage("John Fife")
                .checkThatCurrentEpisodePageMatchItem()
                .openDetailEpisodePageWithEpisodeNameLink()
                .checkThatCurrentEpisodeDetailPageMatchItem()
                .openEpisodePageWithDiagramLink()
                .openConceptMapPageWihAllEpisodesButton()
                .openNodePage("Centralized")
                .openConceptMapPageWihAllEpisodesLink();
    }

    //TODO: add additional more details scenarios
}