package tk.mdogx.concept_map.selenium.elements;

import org.openqa.selenium.By;

//TODO: should be removed, is better to use a separate class 

public interface ItemPageNavigation {
    By allEpisodesLinkLocator = By.xpath("//*[@id='graph']//*[@class='all-episodes']");
    By diagramLocator = By.id("graph");
    By itemInformationLocator = By.id("graph-info");
    By itemsLocator = By.xpath("//*[@class='node']//*[@class='label']");
}
