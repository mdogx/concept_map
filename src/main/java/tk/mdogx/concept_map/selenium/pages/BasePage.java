package tk.mdogx.concept_map.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class BasePage {

    private final WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    protected void checkPageTitle(String pageTitle) {
        Assert.assertEquals(driver.getTitle(), pageTitle, "Page title doesn't match");
    }

    protected void checkPageURL(String pageURL) {
        Assert.assertEquals(driver.getCurrentUrl(), pageURL, "Page URL doesn't match");
    }

    protected String transformItemNameToURL(String itemName) {
        return itemName.toLowerCase().replace(" ", "-");
    }
}