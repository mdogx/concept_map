package tk.mdogx.concept_map.selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import tk.mdogx.concept_map.selenium.Utils;

import java.util.HashSet;

public class ConceptMapPage extends BasePage {

    // Episodes and nodes both are page items

    private final WebDriver driver;
    private final String pageTitle = "Concept map – The Conversation";

    public ConceptMapPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        checkPageTitle(pageTitle);
        checkPageURL(Utils.BASEURL);
    }

    public EpisodePage openEpisodePage(String episodeName) {
        driver.findElement(getEpisodeLocator(episodeName)).click();
        return new EpisodePage(driver, episodeName);
    }

    public NodePage openNodePage(String nodeName) {
        driver.findElement(getNodeLocator(nodeName)).click();
        return new NodePage(driver, nodeName);
    }

    private By getEpisodeLocator(String itemName) {
        return By.xpath("//*[@class = 'episode'][contains(., '" + itemName + "')]");
    }

    private By getNodeLocator(String itemName) {
        return By.xpath("//*[@class='node']//*[@class='label'][contains(., '" + itemName + "')]");
    }

    private HashSet getItems(By locator) {
        HashSet<String> items = new HashSet();
        for (WebElement item : driver.findElements(locator)) {
            items.add(item.getText());
        }
        return items;
    }

    public ConceptMapPage checkThatAllEpisodesAreVisible() {
        Assert.assertTrue(Utils.equalSets(Utils.getEpisodesModel(), getItems(getEpisodeLocator(""))), "Not all episodes are displayed");
        return this;
    }

    public ConceptMapPage checkThatAllNodesAreVisible() {
        Assert.assertTrue(Utils.equalSets(Utils.getNodesModel(), getItems(getNodeLocator(""))), "Not all nodes are displayed");
        return this;
    }
}
