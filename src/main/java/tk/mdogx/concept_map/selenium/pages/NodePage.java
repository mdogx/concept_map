package tk.mdogx.concept_map.selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import tk.mdogx.concept_map.selenium.Utils;
import tk.mdogx.concept_map.selenium.elements.ItemPageNavigation;

public class NodePage extends BasePage implements ItemPageNavigation {

    //TODO: inherit from node page Theme and Perspective pages, if necessary

    private WebDriver driver;
    private final String itemName;

    private final By nodePairLocator = By.xpath("//*[@class='detail']/*[@class='pair']");


    public NodePage(WebDriver driver, String itemName) {
        super(driver);
        this.driver = driver;
        this.itemName = itemName;
        checkPageURL(Utils.BASEURL + transformItemNameToURL(itemName));
    }

    private int getNumberOfItemsOnPage() {
        // TODO need improve accuracy
        return (driver.findElements(itemsLocator).size()-1)/2;
    }

    public ConceptMapPage openConceptMapPageWihAllEpisodesLink() {
        driver.findElement(allEpisodesLinkLocator).click();
        return new ConceptMapPage(driver);
    }

    public NodePage checkThatCurrentEpisodePageMatchItem() {
        checkPageURL(Utils.BASEURL + transformItemNameToURL(itemName));
        return this;
    }

    public NodePage checkThatNumberOfEpisodesAreVisible(double itemsNumber) {
        Assert.assertEquals(itemsNumber, getNumberOfItemsOnPage(), "Not all items of " + itemName + " node page are displayed.");
        return this;
    }

        public NodePage checkThatNodePairMatchExpected(String text) {
        Assert.assertEquals(text, driver.findElement(nodePairLocator).getText(), "Node pair  doesn't match.");
        return this;
    }
}