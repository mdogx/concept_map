package tk.mdogx.concept_map.selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import tk.mdogx.concept_map.selenium.Utils;
import tk.mdogx.concept_map.selenium.elements.ItemPageNavigation;

public class EpisodePage extends BasePage implements ItemPageNavigation {

    private WebDriver driver;
    private final String itemName;

    public EpisodePage(WebDriver driver, String itemName) {
        super(driver);
        this.driver = driver;
        this.itemName = itemName;
        checkPageURL(Utils.BASEURL + transformItemNameToURL(itemName));
    }

    private By getEpisodeNodeLocator() {
        return By.xpath("//*[@class='label' and text() = '" + itemName + "']//parent::*[@class='node']");
    }

    private int getNumberOfItemsOnPage() {
        // TODO need improve method accuracy
        return (driver.findElements(itemsLocator).size()-1)/2;
    }

    public ConceptMapPage openConceptMapPageWihAllEpisodesButton() {
        driver.findElement(allEpisodesLinkLocator).click();
        return new ConceptMapPage(driver);
    }

    public EpisodeDetailPage openDetailEpisodePageWithEpisodeNameLink() {
        driver.findElement(getEpisodeNodeLocator()).click();
        return new EpisodeDetailPage(driver, itemName);
    }

    public EpisodePage checkThatCurrentEpisodePageMatchItem() {
        checkPageURL(Utils.BASEURL + transformItemNameToURL(itemName));
        return this;
    }

    public EpisodePage checkThatNumberOfNodesAreVisible(double itemsNumber) {
        Assert.assertEquals(itemsNumber, getNumberOfItemsOnPage(), "Not all items of " + itemName + " episode page are displayed");
        return this;
    }

    public EpisodePage checkEpisodeInformation(String text){
        System.out.println( driver.findElement(itemInformationLocator).getText());
        Assert.assertEquals(text, driver.findElement(itemInformationLocator).getText(), "Episode information doesn't match");
        return this;
    }


}