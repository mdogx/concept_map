package tk.mdogx.concept_map.selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import tk.mdogx.concept_map.selenium.Utils;

public class EpisodeDetailPage extends BasePage {

    private WebDriver driver;
    private final String itemName;

    //TODO: add all page elements
    private final By episodePageLocator = By.xpath("//div[@id='con-sidebar']/a[contains(@href,'/concept-map/#')]");
    private final By contentLocator = By.xpath("//div[@class='entry-content']/p[text()]");
    private final By tagsLocator = By.xpath("//div[@class='entry-meta']");

    public EpisodeDetailPage(WebDriver driver, String itemName) {
        super(driver);
        this.driver = driver;
        this.itemName = itemName;
        checkPageURL(Utils.BASEURL + transformItemNameToURL(itemName));
    }

    public EpisodePage openEpisodePageWithDiagramLink() {
        driver.findElement(episodePageLocator).click();
        return new EpisodePage(driver, itemName);
    }

    @Override
    protected void checkPageURL(String pageURL) {
        Assert.assertTrue(driver.getCurrentUrl().contains(transformItemNameToURL(itemName)), "Page URL doesn't match");
    }

    public EpisodeDetailPage checkThatCurrentEpisodeDetailPageMatchItem() {
        checkPageURL(Utils.BASEURL + transformItemNameToURL(itemName));
        return this;
    }

    public EpisodeDetailPage checkThatContentOfEpisodeDetailPageMatchExpected(String text) {
        Assert.assertEquals(text, driver.findElement(contentLocator).getText(), "Episode content doesn't match");
        return this;
    }

    public EpisodeDetailPage checkThatTagsOfEpisodeDetailPageMatchExpected(String text) {
        Assert.assertEquals(text, driver.findElement(tagsLocator).getText(), "Tags content doesn't match");
        return this;
    }

    //TODO: add methods for check different fields of Episode Detail page
}