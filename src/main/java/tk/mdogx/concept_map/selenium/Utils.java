package tk.mdogx.concept_map.selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.*;
import java.util.*;

public class Utils {

    private static WebDriver webDriver;

    public final static String BROWSER = getPropertyByKey("browser").toLowerCase();
    public final static String BASEURL = getPropertyByKey("baseUrl").toLowerCase();
    public final static Long TIMEOUT = Long.parseLong(getPropertyByKey("timeout"));

    private final static String properties = "src/main/resources/test.properties";
    private final static String episodes = "src/main/resources/episodes.txt";
    private final static String nodes = "src/main/resources/nodes.txt";

    //TODO: add additional browsers if needed
    public static WebDriver getDriver() {
        if (webDriver != null) {
            return webDriver;
        }
        switch (BROWSER) {
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                webDriver = new FirefoxDriver();
                break;
            case "chrome":
                WebDriverManager.chromedriver().setup();
                webDriver = new ChromeDriver();
                break;
        }
        return webDriver;
    }

    public static String getPropertyByKey(String key) {
        // Take properties from file
        Properties prop = new Properties();
        try (FileInputStream fis = new FileInputStream(properties);
             InputStreamReader isr = new InputStreamReader(fis, "UTF-8")) {
            prop.load(isr);

        } catch (IOException e) {
        }
        return prop.getProperty(key);
    }

    private static HashSet getDataFromFile(String source) {
        HashSet lines = new HashSet<>();
        try {
            BufferedReader txt = new BufferedReader(new FileReader(source));
            String line;
            while ((line = txt.readLine()) != null) {
                lines.add(line);
            }
            txt.close();
        } catch (IOException e) {
        }
        return lines;
    }

    public static HashSet getEpisodesModel() {
        return getDataFromFile(episodes);
    }

    public static HashSet getNodesModel() {
        return getDataFromFile(nodes);
    }

    public static boolean equalSets(Set<String> one, Set<String> two) {
        if (one == two) {
            return true;
        }

        if (one != null && two != null) {
            return one.containsAll(two);
        } else {
            return false;
        }
    }
}