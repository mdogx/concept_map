# Overview

This repository contains automation framework for testing http://www.findtheconversation.com/concept-map/#

Used Java, Selenium, Maven.
Test designed to run with TestNG and using page object pattern.

Use test.properties for configure test:

*  specify start page
*  select chrome/firefox webdriver
*  specify waiting timeout.

# Implementation

Implemented interaction with the following pages:

* main concept map page
* any episode page
* any node page
* pages with detailed description of episode.

# Scenarios 

Created tests with smoke validation of basic features.